# Monitoring Euler

## Metriche
- `node_exporter`: esporta le metriche hardware e del sistema operativo, pertanto deve montare la `/` e altre cartelle sensibili. Non so quanto sia problematico per la sicurezza, ma in ogni caso ha una porta esposta solo nella rete locale di docker.
- `prometheus`: time series database che fa le pull al `node_exporter`

## Log
- `promtail`: client per `loki` che il push dei log. Monta la `/var/log` ma anche lui la espone solo alla rete interna.
- `loki`: log aggregation tool e database

## Dashboards
- `grafana`: dashboards fatte a partire dai database (`prometheus`) e motore di ricerca per fare query ai database (`prometheus` e `loki`). Esposto solo a `localhost:3000`
